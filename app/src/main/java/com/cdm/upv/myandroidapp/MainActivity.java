package com.cdm.upv.myandroidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    Button button;
    Button button2;
    Button button3;
    Button button4;
    ImageView image;
    public final static String EXTRA_MESSAGE = "com.cdm.upv.myandroidapp.MESSAGE";
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addListenerOnButton();
    }

    public void addListenerOnButton(){
        intent = new Intent(this, Main2Activity.class);
        image = (ImageView)findViewById(R.id.imageView1);

        button = (Button)findViewById(R.id.btnChangeImage1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //image.setImageResource(R.drawable.manzana2);
                intent.putExtra(EXTRA_MESSAGE, "1");
                startActivity(intent);
            }
        });

        //image = (ImageView)findViewById(R.id.imageView1);

        button2 = (Button)findViewById(R.id.btnChangeImage2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //image.setImageResource(R.drawable.pina2);
                intent.putExtra(EXTRA_MESSAGE, "2");
                startActivity(intent);
            }
        });


        //image = (ImageView)findViewById(R.id.imageView1);

        button3 = (Button)findViewById(R.id.btnChangeImage3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //image.setImageResource(R.drawable.sandia2);
                intent.putExtra(EXTRA_MESSAGE, "3");
                startActivity(intent);
            }
        });


        //image = (ImageView)findViewById(R.id.imageView1);

        button4 = (Button)findViewById(R.id.btnChangeImage4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //image.setImageResource(R.drawable.uva2);
                intent.putExtra(EXTRA_MESSAGE, "4");
                startActivity(intent);
            }
        });

    }
}

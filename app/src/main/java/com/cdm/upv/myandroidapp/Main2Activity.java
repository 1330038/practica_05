package com.cdm.upv.myandroidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {

    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        int num=Integer.parseInt(message);
        image = (ImageView) findViewById(R.id.imageView1);

        if(num == 1){
            image.setImageResource(R.drawable.manzana2);
        }else if(num==2){
            image.setImageResource(R.drawable.pina2);
        }else if(num==3){
            image.setImageResource(R.drawable.sandia2);
        }else{
            image.setImageResource(R.drawable.uva2);
        }

    }
}
